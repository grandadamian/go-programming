package main

import "fmt"

func main() {
	// unbufferd channel
	ca := make(chan int)

	ca <- 42
	fmt.Println(<-ca)

}
