package main

import (
	"fmt"
	"runtime"
)

func main() {

	fmt.Println("Sistema operativo: %v\n Arquitectura:%v\n", runtime.GOOS, runtime.GOARCH)

}
